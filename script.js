// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
// Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this.name;
    }
    
    set name(value) {
        this.name = value;
    } 

  

    get salary() {
        return this.salary;
    }

    set salary(value) {
        this.salary = value;
    } 
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
    super(name, age, salary * 3) 
        this._lang = lang
    }
    get lang() {
        return this.lang;
    }

    set lang(value){
        this.lang = value;
    }

    get salary() {
        return this.salary;
    }

    set salary(value) {
        this.salary = value;
    }

}

let programmerOgj = new Programmer('Vlad', 'White', 55000, ['Russian', 'Ukrainian', 'English']);
let programmerOgj2 = new Programmer('Alex', 'Black', 155000, 'English');
let programmerOgj3 = new Programmer('Vasya', 'Pupkin', 5000, 'Russian');
let programmerOgj4 = new Programmer('Emma', 'Garcia', 1000000, ['Russian', 'Ukrainian', 'English']);

console.log(programmerOgj);
console.log(programmerOgj2);
console.log(programmerOgj3);
console.log(programmerOgj4);


// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Для чого потрібно викликати super() у конструкторі класу-нащадка? Для того, щоб витягти параметри у классу-батька Якщо об'єкт B є прототипом об'єкта A, то всякий раз, коли у B є властивість, наприклад колір, A успадкує той же самий колір, якщо інше не вказано явно. 